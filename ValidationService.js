import { Navigate } from "react-router-dom";


export function ValidationLogin (username) {
    const errorsLogin = [];
    if (!username) {
        errorsLogin.push("Поле не должно быть пустым")
    } else if (username.length < 4) {
        errorsLogin.push("Логин должен содержать не менее 4-х символов")
    } 
    return errorsLogin;
}

export function ValidationPass (password) {
    const errorsPass = [];
    if (!password) {
        errorsPass.push("Поле не должно быть пустым")
    } else if (password.length < 4) {
        errorsPass.push("Пароль должен содержать не менее 4-х символов") 
    }
    return errorsPass;
}

export function ValidationReg (username, password) {
    const validReg = []
        if (username === JSON.parse(localStorage.getItem(`user`))?.login) {
            validReg.push(`Пользователь с этим логином уже зарегестрирован`)
        } else if (username != JSON.parse(localStorage.getItem(`user`))?.login) {
            validReg.push(`Вы зарегестрированы`)
            localStorage.setItem('user', JSON.stringify({
                login: username,
                password: password
            }))
        }
        return validReg;
    }

export function ValidationUser (username, password) {
    const authUser = []
        if (((username === JSON.parse(localStorage.getItem('user'))?.login)) && ((password === JSON.parse(localStorage.getItem('user'))?.password))) {
            authUser.push(`Вы авторизовались`)
            return (<Navigate to='/'/>)
        } else if (((username != JSON.parse(localStorage.getItem('user'))?.login)) || ((password != JSON.parse(localStorage.getItem('user'))?.password))) {
            authUser.push(`Логин или пароль неверен`)
        }
        return authUser;
}



