import { Outlet, Navigate } from "react-router-dom";
function PrivateRoute () {

    let login = (localStorage.getItem('user'))
    let auth = login
    return (
        auth ? <Outlet/> : <Navigate to='authorithation'/>
    )
} 

export default PrivateRoute
