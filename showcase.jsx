import Header from '../components/app-header/app-header'
import Shop from '../components/card/cards'
function ShowCase(props) {
    return (
        <>
        <Header/>
        <Shop data={props.services}/>
        </>
    )
}

export default ShowCase